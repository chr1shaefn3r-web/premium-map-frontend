import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from "@angular/forms";

import { AppRoutingModule } from "./app-routing.module";
import { SearchService } from './search.service';
import { AppComponent } from "./app.component";
import { MapComponent } from "./map/map.component";
import { DropdownComponent } from "./dropdown/dropdown.component";
import { PopupComponent } from "./popup/popup.component";

@NgModule({
  declarations: [
    AppComponent,
    MapComponent,
    DropdownComponent,
    PopupComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule,
  ],
  providers: [SearchService],
  bootstrap: [AppComponent],
})
export class AppModule {}
